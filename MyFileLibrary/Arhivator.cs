﻿using System.IO;
using System.IO.Compression;

namespace MyFileLibrary
{
    public class Arhivator
    {
        private string directoryPath;

        public Arhivator() { directoryPath = @".\temp"; }

        public void Compress(string sourceFile, string compressedFile)
        {
            using (FileStream sourceStream = new FileStream(sourceFile,
              FileMode.OpenOrCreate))
            {

                using (FileStream targetStream = File.Create(compressedFile + ".gzar"))
                {

                    using (GZipStream compressionStream = new GZipStream(targetStream, CompressionMode.Compress))
                    {
                        sourceStream.CopyTo(compressionStream);
                    }

                }

            }

        }

        public void DeCompress(string sourceFile, string compressedFile)
        {
            using (FileStream originalFileStream = new FileStream(sourceFile, FileMode.OpenOrCreate))
            {

                using (FileStream decompressedFileStream = File.Create(compressedFile.Substring(0, compressedFile.Length - 5)))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                    }
                }
            }
        }
    }


}



