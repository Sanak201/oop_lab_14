﻿using System.IO;


namespace MyFileLibrary
{
    public class Crypter
    {
        private string password;
        private string filepaths;

        public Crypter() 
        {
           
        }

        public void CryptFile(string Filepath,string Password) 
        {
            FileStream flstrIN = File.OpenRead(Filepath);
            FileStream flstrOUT = File.OpenWrite(Filepath+".crypt");
            int symbol;
            int PasPosition=0;
            while ((symbol = flstrIN.ReadByte()) != -1) 
            {
                symbol = symbol ^ Password[PasPosition];
                PasPosition++;
                flstrOUT.WriteByte((byte)symbol);
                if (PasPosition >= Password.Length) PasPosition = 0;
            }

            flstrIN.Close();
            flstrOUT.Close();
        }

        public void DecriptFile(string Filepath, string Password) 
        {
            FileStream flstrIN = File.OpenRead(Filepath);
            FileStream flstrOUT = File.OpenWrite(Filepath.Substring(0, Filepath.Length - 6) + ".decrypt");
            int symbol;
            int PasPosition = 0;
            while ((symbol = flstrIN.ReadByte()) != -1)
            {
                symbol = symbol ^ Password[PasPosition];
                PasPosition++;
                flstrOUT.WriteByte((byte)symbol);
                if (PasPosition >= Password.Length) PasPosition = 0;
            }

            flstrIN.Close();
            flstrOUT.Close();
        }
        
    }
}
