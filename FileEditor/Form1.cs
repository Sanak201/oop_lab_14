﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyFileLibrary;

namespace FileEditor
{
    public partial class Form1 : Form
    {
        List<string> filesPathListForFilePath;
        List<string> filesPathListForArhiveListBox;
        public Form1()
        {
            InitializeComponent();
            filesPathListForFilePath = new List<string>();
            filesPathListForArhiveListBox = new List<string>();

            UnvisibleAllPanels();
        }

        
        
        private void FilesList_DragEnter(object sender, DragEventArgs e)
        {
            
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) 
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void FilesList_DragDrop(object sender, DragEventArgs e)
        {
            LisBoxDragAndDrop(ref sender, e, ref filesPathListForFilePath);
        }

        private void AddFile_Click(object sender, EventArgs e)
        {
            MyOpenDialogSkript(ref FilesList, ref filesPathListForFilePath);
        }

        private void ArhiveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            UnvisibleAllPanels();
            ArhivePanel.Visible = true;
        }

        private void EncrypToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnvisibleAllPanels();
            EncrypPanel.Visible = true;
        }

        private void UnEncrypToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            UnvisibleAllPanels();
            UnEncryptPanel.Visible = true;
        }

        private void UnArhiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnvisibleAllPanels();
            UnArhivePanel.Visible = true;
        }

        void UnvisibleAllPanels() 
        {
            ArhivePanel.Visible = false;
            UnArhivePanel.Visible = false;
            EncrypPanel.Visible = false;
            UnEncryptPanel.Visible = false;
        }

        private void CryptButton_Click(object sender, EventArgs e)
        {
            Crypter crpt = new Crypter();

            crpt.CryptFile(filesPathListForFilePath[FilesList.SelectedIndex], CryptTextBox.Text);
        }

        private void DecriptButton_Click(object sender, EventArgs e)
        {
            Crypter crpt = new Crypter();
            crpt.DecriptFile(filesPathListForFilePath[FilesList.SelectedIndex], DecryptTextBox.Text);
        }

       
        void LisBoxDragAndDrop(ref object sender, DragEventArgs e, ref List<string> list)
        {
            string[] filestrs = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in filestrs)
            {
                (sender as ListBox).Items.Add(Path.GetFileName(file));
                list.Add(file);
            }
        }
        void MyOpenDialogSkript(ref ListBox lisB, ref List<string> str)
        {
            OpenFileDialog FileDialog = new OpenFileDialog();
            if (FileDialog.ShowDialog(this) == DialogResult.OK)
            {
                foreach (string file in FileDialog.FileNames)
                {
                    lisB.Items.Add(Path.GetFileName(file));
                    str.Add(file);
                }
            }
        }

        private void ArhiveSaveToPathButton_Click(object sender, EventArgs e)
        {
            if (FilesList.SelectedIndex != -1)
            {
                Arhivator arh = new Arhivator();
                arh.Compress(filesPathListForFilePath[FilesList.SelectedIndex], filesPathListForFilePath[FilesList.SelectedIndex]);
            }
        }

        private void DecompressButton_Click(object sender, EventArgs e)
        {
            if (FilesList.SelectedIndex != -1)
            {
                Arhivator arh = new Arhivator();
                arh.DeCompress(filesPathListForFilePath[FilesList.SelectedIndex], filesPathListForFilePath[FilesList.SelectedIndex]);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
