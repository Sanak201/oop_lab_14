﻿
namespace FileEditor
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddFile = new System.Windows.Forms.Button();
            this.FilesList = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ArhiveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.UnArhiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EncrypToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UnEncrypToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ArhivePanel = new System.Windows.Forms.Panel();
            this.ArhiveSaveToPathButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.UnArhivePanel = new System.Windows.Forms.Panel();
            this.DecompressButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.EncrypPanel = new System.Windows.Forms.Panel();
            this.CryptButton = new System.Windows.Forms.Button();
            this.CryptTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.UnEncryptPanel = new System.Windows.Forms.Panel();
            this.DecriptButton = new System.Windows.Forms.Button();
            this.DecryptTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.ArhivePanel.SuspendLayout();
            this.UnArhivePanel.SuspendLayout();
            this.EncrypPanel.SuspendLayout();
            this.UnEncryptPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // AddFile
            // 
            this.AddFile.Location = new System.Drawing.Point(9, 150);
            this.AddFile.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.AddFile.Name = "AddFile";
            this.AddFile.Size = new System.Drawing.Size(119, 25);
            this.AddFile.TabIndex = 1;
            this.AddFile.Text = "Додати файл";
            this.AddFile.UseVisualStyleBackColor = true;
            this.AddFile.Click += new System.EventHandler(this.AddFile_Click);
            // 
            // FilesList
            // 
            this.FilesList.AllowDrop = true;
            this.FilesList.FormattingEnabled = true;
            this.FilesList.Location = new System.Drawing.Point(9, 37);
            this.FilesList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FilesList.Name = "FilesList";
            this.FilesList.Size = new System.Drawing.Size(251, 108);
            this.FilesList.TabIndex = 2;
            this.FilesList.DragDrop += new System.Windows.Forms.DragEventHandler(this.FilesList_DragDrop);
            this.FilesList.DragEnter += new System.Windows.Forms.DragEventHandler(this.FilesList_DragEnter);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(603, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // FileToolStripMenuItem
            // 
            this.FileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ArhiveToolStripMenuItem1,
            this.UnArhiveToolStripMenuItem,
            this.EncrypToolStripMenuItem,
            this.UnEncrypToolStripMenuItem});
            this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
            this.FileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.FileToolStripMenuItem.Text = "Файл";
            // 
            // ArhiveToolStripMenuItem1
            // 
            this.ArhiveToolStripMenuItem1.Name = "ArhiveToolStripMenuItem1";
            this.ArhiveToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.ArhiveToolStripMenuItem1.Text = "Архівувати";
            this.ArhiveToolStripMenuItem1.Click += new System.EventHandler(this.ArhiveToolStripMenuItem1_Click);
            // 
            // UnArhiveToolStripMenuItem
            // 
            this.UnArhiveToolStripMenuItem.Name = "UnArhiveToolStripMenuItem";
            this.UnArhiveToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.UnArhiveToolStripMenuItem.Text = "Розархівувати";
            this.UnArhiveToolStripMenuItem.Click += new System.EventHandler(this.UnArhiveToolStripMenuItem_Click);
            // 
            // EncrypToolStripMenuItem
            // 
            this.EncrypToolStripMenuItem.Name = "EncrypToolStripMenuItem";
            this.EncrypToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.EncrypToolStripMenuItem.Text = "Шифрувати";
            this.EncrypToolStripMenuItem.Click += new System.EventHandler(this.EncrypToolStripMenuItem_Click);
            // 
            // UnEncrypToolStripMenuItem
            // 
            this.UnEncrypToolStripMenuItem.Name = "UnEncrypToolStripMenuItem";
            this.UnEncrypToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.UnEncrypToolStripMenuItem.Text = "Розшифрувати";
            this.UnEncrypToolStripMenuItem.Click += new System.EventHandler(this.UnEncrypToolStripMenuItem_Click_1);
            // 
            // ArhivePanel
            // 
            this.ArhivePanel.BackColor = System.Drawing.Color.Lime;
            this.ArhivePanel.Controls.Add(this.ArhiveSaveToPathButton);
            this.ArhivePanel.Controls.Add(this.label1);
            this.ArhivePanel.Location = new System.Drawing.Point(325, 25);
            this.ArhivePanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ArhivePanel.Name = "ArhivePanel";
            this.ArhivePanel.Size = new System.Drawing.Size(231, 122);
            this.ArhivePanel.TabIndex = 5;
            // 
            // ArhiveSaveToPathButton
            // 
            this.ArhiveSaveToPathButton.Location = new System.Drawing.Point(11, 39);
            this.ArhiveSaveToPathButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ArhiveSaveToPathButton.Name = "ArhiveSaveToPathButton";
            this.ArhiveSaveToPathButton.Size = new System.Drawing.Size(119, 28);
            this.ArhiveSaveToPathButton.TabIndex = 5;
            this.ArhiveSaveToPathButton.Text = "Заархівувати файл";
            this.ArhiveSaveToPathButton.UseVisualStyleBackColor = true;
            this.ArhiveSaveToPathButton.Click += new System.EventHandler(this.ArhiveSaveToPathButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Архівувати";
            // 
            // UnArhivePanel
            // 
            this.UnArhivePanel.BackColor = System.Drawing.Color.Lime;
            this.UnArhivePanel.Controls.Add(this.DecompressButton);
            this.UnArhivePanel.Controls.Add(this.label2);
            this.UnArhivePanel.Location = new System.Drawing.Point(275, 35);
            this.UnArhivePanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.UnArhivePanel.Name = "UnArhivePanel";
            this.UnArhivePanel.Size = new System.Drawing.Size(231, 122);
            this.UnArhivePanel.TabIndex = 6;
            // 
            // DecompressButton
            // 
            this.DecompressButton.Location = new System.Drawing.Point(14, 53);
            this.DecompressButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DecompressButton.Name = "DecompressButton";
            this.DecompressButton.Size = new System.Drawing.Size(119, 28);
            this.DecompressButton.TabIndex = 6;
            this.DecompressButton.Text = "Заархівувати файл";
            this.DecompressButton.UseVisualStyleBackColor = true;
            this.DecompressButton.Click += new System.EventHandler(this.DecompressButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Розархівувати";
            // 
            // EncrypPanel
            // 
            this.EncrypPanel.BackColor = System.Drawing.Color.Lime;
            this.EncrypPanel.Controls.Add(this.CryptButton);
            this.EncrypPanel.Controls.Add(this.CryptTextBox);
            this.EncrypPanel.Controls.Add(this.label5);
            this.EncrypPanel.Controls.Add(this.label3);
            this.EncrypPanel.Location = new System.Drawing.Point(288, 28);
            this.EncrypPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.EncrypPanel.Name = "EncrypPanel";
            this.EncrypPanel.Size = new System.Drawing.Size(231, 122);
            this.EncrypPanel.TabIndex = 6;
            // 
            // CryptButton
            // 
            this.CryptButton.Location = new System.Drawing.Point(16, 79);
            this.CryptButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.CryptButton.Name = "CryptButton";
            this.CryptButton.Size = new System.Drawing.Size(195, 30);
            this.CryptButton.TabIndex = 3;
            this.CryptButton.Text = "Зашифрувати";
            this.CryptButton.UseVisualStyleBackColor = true;
            this.CryptButton.Click += new System.EventHandler(this.CryptButton_Click);
            // 
            // CryptTextBox
            // 
            this.CryptTextBox.Location = new System.Drawing.Point(51, 44);
            this.CryptTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.CryptTextBox.Name = "CryptTextBox";
            this.CryptTextBox.Size = new System.Drawing.Size(161, 20);
            this.CryptTextBox.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 44);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Ключ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Зашифрувати";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // UnEncryptPanel
            // 
            this.UnEncryptPanel.BackColor = System.Drawing.Color.Lime;
            this.UnEncryptPanel.Controls.Add(this.DecriptButton);
            this.UnEncryptPanel.Controls.Add(this.DecryptTextBox);
            this.UnEncryptPanel.Controls.Add(this.label6);
            this.UnEncryptPanel.Controls.Add(this.label4);
            this.UnEncryptPanel.Location = new System.Drawing.Point(362, 23);
            this.UnEncryptPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.UnEncryptPanel.Name = "UnEncryptPanel";
            this.UnEncryptPanel.Size = new System.Drawing.Size(231, 122);
            this.UnEncryptPanel.TabIndex = 7;
            // 
            // DecriptButton
            // 
            this.DecriptButton.Location = new System.Drawing.Point(13, 79);
            this.DecriptButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DecriptButton.Name = "DecriptButton";
            this.DecriptButton.Size = new System.Drawing.Size(195, 30);
            this.DecriptButton.TabIndex = 6;
            this.DecriptButton.Text = "Розшифрувати";
            this.DecriptButton.UseVisualStyleBackColor = true;
            this.DecriptButton.Click += new System.EventHandler(this.DecriptButton_Click);
            // 
            // DecryptTextBox
            // 
            this.DecryptTextBox.Location = new System.Drawing.Point(48, 44);
            this.DecryptTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DecryptTextBox.Name = "DecryptTextBox";
            this.DecryptTextBox.Size = new System.Drawing.Size(161, 20);
            this.DecryptTextBox.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 44);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Ключ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 12);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Розшифрувати";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 228);
            this.Controls.Add(this.ArhivePanel);
            this.Controls.Add(this.EncrypPanel);
            this.Controls.Add(this.UnArhivePanel);
            this.Controls.Add(this.UnEncryptPanel);
            this.Controls.Add(this.FilesList);
            this.Controls.Add(this.AddFile);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximumSize = new System.Drawing.Size(619, 267);
            this.MinimumSize = new System.Drawing.Size(619, 267);
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ArhivePanel.ResumeLayout(false);
            this.ArhivePanel.PerformLayout();
            this.UnArhivePanel.ResumeLayout(false);
            this.UnArhivePanel.PerformLayout();
            this.EncrypPanel.ResumeLayout(false);
            this.EncrypPanel.PerformLayout();
            this.UnEncryptPanel.ResumeLayout(false);
            this.UnEncryptPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button AddFile;
        private System.Windows.Forms.ListBox FilesList;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ArhiveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem UnArhiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EncrypToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UnEncrypToolStripMenuItem;
        private System.Windows.Forms.Panel ArhivePanel;
        private System.Windows.Forms.Panel UnArhivePanel;
        private System.Windows.Forms.Panel EncrypPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel UnEncryptPanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button CryptButton;
        private System.Windows.Forms.TextBox CryptTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button DecriptButton;
        private System.Windows.Forms.TextBox DecryptTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ArhiveSaveToPathButton;
        private System.Windows.Forms.Button DecompressButton;
    }
}

